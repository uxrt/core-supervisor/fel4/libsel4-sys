#!/bin/sh
TMPFILE="`mktemp`"

trap "rm -f $TMPFILE" HUP INT EXIT

sed 's/pub type seL4_Word = [0-9A-Za-z_][0-9A-Za-z_]*/pub type seL4_Word = usize/g' < "$1" > "$TMPFILE"
mv "$TMPFILE" "$1"
